//! Wasm Bindgen example to React/typescript
#![warn(clippy::all)]
#![warn(clippy::pedantic)]
#![warn(clippy::cargo)]
#![warn(clippy::restriction)]
#![warn(clippy::nursery)]
#![warn(missing_docs)]
#![deny(unsafe_op_in_unsafe_fn)]

use wasm_bindgen::prelude::*;

// todo: example for running typescript/javascript code here?

/// Simple add function
#[wasm_bindgen]
pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
