module.exports = {
  plugins: [
    '@typescript-eslint',
    'import',
    'promise',
  ],
  extends: [
    'airbnb',
    'airbnb/hooks',
    'airbnb-typescript',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
  ],
  parserOptions: {
    project: './tsconfig.json',
  },
  rules: {
    'max-len': ['warn', {
      code: 120,
    }],
    '@typescript-eslint/no-unused-vars': ['warn', {
      argsIgnorePattern: '^_',
    }],
    'react/function-component-definition': ['error', {
      namedComponents: 'arrow-function',
    }],
  },
};
